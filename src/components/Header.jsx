import { Link } from "../i18n";
import { Menu, Icon } from "semantic-ui-react";
import LocalePicker from "./LocalePicker";
import { withTranslation } from "../i18n";
import { useRouter } from "next/router";

const Header = ({ t }) => {
  const { pathname } = useRouter();

  const buttons = [
    { path: "/", text: "home", icon: "home" },
    { path: "/about", text: "about", icon: "question" },
  ];

  return (
    <Menu inverted>
      {buttons.map((button) => (
        <Link key={button.text} href={button.path}>
          <Menu.Item active={pathname === button.path}>
            <Icon name={button.icon} />
            {t(button.text)}
          </Menu.Item>
        </Link>
      ))}
      <LocalePicker />
    </Menu>
  );
};

Header.getInitialProps = async () => ({
  namespacesRequired: ["common"],
});

export default withTranslation("common")(Header);
