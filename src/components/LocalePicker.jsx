import { Menu, Flag } from "semantic-ui-react";
import { i18n, config } from "../../src/i18n";
import { I18nContext } from "next-i18next";
import { useContext } from "react";

const LocalePicker = () => {
  const {
    i18n: { language },
  } = useContext(I18nContext);

  return (
    <Menu.Menu position="right">
      {config.allLanguages.map((locale) => (
        <Menu.Item
          active={locale === language}
          key={locale}
          onClick={() => i18n.changeLanguage(locale)}
        >
          <Flag name={locale === "en" ? "us" : locale} />
          {` ${locale.toUpperCase()}`}
        </Menu.Item>
      ))}
    </Menu.Menu>
  );
};

export default LocalePicker;
