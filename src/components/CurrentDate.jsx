import { Segment, Label } from "semantic-ui-react";
import { DateTime } from "luxon";
import { I18nContext } from "next-i18next";
import { useContext } from "react";

const CurrentDate = ({}) => {
  const {
    i18n: { language },
  } = useContext(I18nContext);

  const config = {
    ...DateTime.DATETIME_HUGE,
    timeZoneName: "short",
  };

  const dt = DateTime.local()
    .setLocale(language)
    .setZone("utc")
    .toLocaleString(config);

  return (
    <Segment>
      <Label content={dt} icon="clock outline" />
    </Segment>
  );
};

export default CurrentDate;
