const NextI18Next = require("next-i18next").default;
const path = require("path");

module.exports = new NextI18Next({
  defaultLanguage: "en",
  otherLanguages: ["nl", "no", "es"],
  localeSubpaths: {
    nl: "nl",
    no: "no",
    es: "es",
  },
  localePath: path.resolve("./public/static/locales"),
});
