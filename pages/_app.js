import App from "next/app";
import "semantic-ui-css/semantic.min.css";
import "../styles/globals.css";
import { appWithTranslation } from "../src/i18n";
import Header from "../src/components/Header";
import { Container, Segment, Button, Icon } from "semantic-ui-react";
import CurrentDate from "../src/components/CurrentDate";

const MyApp = ({ Component, pageProps }) => {
  return (
    <>
      <div style={{ height: 20 }} />
      <Container>
        <Header />
        <CurrentDate />
        <Segment>
          <Component {...pageProps} />
        </Segment>
        <div style={{ textAlign: "center" }}>
          <a href="https://gerardvanderput.com" target="_blank">
            <Button icon size="mini" labelPosition="left">
              <Icon name="heart" color="red" />
              Gerard van der Put.com
            </Button>
          </a>
        </div>
      </Container>
    </>
  );
};

async function getProps(appContext) {
  return await App.getInitialProps(appContext);
}

MyApp.getInitialProps = getProps;

export default appWithTranslation(MyApp);
