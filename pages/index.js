import { withTranslation } from "../src/i18n";
import { Header } from "semantic-ui-react";

const Home = ({ t }) => {
  return (
    <>
      <Header as="h1">{t("home")}</Header>
      <div>{t("welcomeMessage")}</div>
    </>
  );
};

Home.getInitialProps = async () => ({
  namespacesRequired: ["common"],
});

export default withTranslation("common")(Home);
