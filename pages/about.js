import { withTranslation } from "../src/i18n";
import { Header } from "semantic-ui-react";

const About = ({ t }) => {
  return (
    <>
      <Header as="h1">{t("common:about")}</Header>
      <div>{t("aboutMessage")}</div>
    </>
  );
};

About.getInitialProps = async () => ({
  namespacesRequired: ["about", "common"],
});

export default withTranslation(["about", "common"])(About);
